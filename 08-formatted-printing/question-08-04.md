# Question 8.4

> Use the `*` syntax described in the `Printf` module documentation to write a function which can print a table of integers to a given width.
> For example, given width 10, we might see:
> ```text
> (         1)
> (        23)
> (     33241)
> (         0)
> ```

---

We can implement the described function as follows:
```ocaml
let print_table w l =
  let print_item i =
    Printf.printf "(%*i)\n" w i
  in
  List.iter print_item l
```
