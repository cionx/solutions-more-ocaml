# Question 8.2

> Write a function which, given a string, returns another string which represents the first using hexadecimal numbers.
> For example, the input string `Hello` should yield the output `48656c6c6f` since `'H'` has ASCII code `0x48` and so on.

---

We can implement the described function as follows:
```ocaml
(** [char_list_of_string s] is the list [[s.[0]; s.[1]; ...; s.[l - 1]]] where
    [l] is [String.length s]. *)
let char_list_of_string s =
  String.fold_right (fun c a -> c :: a) s []

let hexadecimal_of_string s =
  s
  |> char_list_of_string
  |> List.map int_of_char
  |> List.map (Printf.sprintf "%x")
  |> String.concat ""
```
