let print_table w l =
  let print_item i =
    Printf.printf "(%*i)\n" w i
  in
  List.iter print_item l
