let cycle_string_of_pair_list l =
  let cycle_list_of_list l =
    l @ [List.hd l]
  in
  let string_of_pair (x, y) =
    Printf.sprintf "(%i, %i)" x y
  in
  match l with
  | [] -> failwith "cycle_string_of_pair_list: empty list"
  | l ->
      l
      |> cycle_list_of_list
      |> List.map string_of_pair
      |> String.concat " --> "



(** Testing *)

let _ = cycle_string_of_pair_list [(1, 2); (5, 6); (6, 6); (7, 5)]
let _ = cycle_string_of_pair_list [(1, 1)]
let _ = cycle_string_of_pair_list []
