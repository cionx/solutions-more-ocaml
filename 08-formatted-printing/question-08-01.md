# Question 8.1

> Given a list of pairs of integers such as `[(1, 2); (5, 6); (6, 6); (7, 5)]`, write a function to return a string representation such as `"(1, 2) --> (5, 6) --> (6, 6) --> (7, 5) --> (1, 2)"`.

---

We implement the described function as follows:
```ocaml
let cycle_string_of_pair_list l =
  let cycle_list_of_list l =
    l @ [List.hd l]
  in
  let string_of_pair (x, y) =
    Printf.sprintf "(%i, %i)" x y
  in
  match l with
  | [] -> failwith "cycle_string_of_pair_list: empty list"
  | l ->
      l
      |> cycle_list_of_list
      |> List.map string_of_pair
      |> String.concat " --> "
```
