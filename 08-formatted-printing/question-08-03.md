# Question 8.3

> Why does the following code cause a type error?
> ```text
> # let mkstring () = "string";;
> val mkstring : unit -> string = <fun>
> # Printf.printf (mkstring ());
>   ;;
> Error: This expression has type string but an expression was expected of type
> ('a, out_channel, unit) format =
> ('a, out_channel, unit, unit, unit, unit) format6
> ```

The problem is probably that the format string needs to be known at compile/interpretation time.
In the given situation we could fix the problem by
- using `print_string (mkstring ())` instead, or
- using `Printf.printf "%s" (mkstring ())` instead.
