(** [char_list_of_string s] is the list [[s.[0]; s.[1]; ...; s.[l - 1]]] where
    [l] is [String.length s]. *)
let char_list_of_string s =
  String.fold_right (fun c a -> c :: a) s []

let hexadecimal_of_string s =
  s
  |> char_list_of_string
  |> List.map int_of_char
  |> List.map (Printf.sprintf "%x")
  |> String.concat ""



(** Testing *)

let _ = hexadecimal_of_string "Hello"
