# Question 9.5

> Add a labelled argument of type `bool` whose value, when `true`, indicates a case-insensitive search.

---

We modify the function `search` as follows:
```ocaml
let rec search ?(case_insensitive = false) p s =
  if case_insensitive
  then search (String.lowercase_ascii p) (String.lowercase_ascii s)
  else search' 0 p s
```
