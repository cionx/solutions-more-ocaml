# Question 9.1

> Write a function to return the number of matches of one string in another a) when all matches are considered and b) when only non-overlapping matches are considered.

---

### a)

This function can be implemented in terms of the function `at` as follows:
```ocaml
let match_num p s =
  let counter = ref 0 in
  for n = 0 to String.length s - 1 do
    if at p 0 s n then counter := !counter + 1
  done;
  !counter
```

### b)

We modify the function `at` to not only return whether a match was found, but also how much of the input string was used up to get this match.
```ocaml
let rec at_end p pp s sp =
  if pp > String.length p - 1 then (true, sp)             (* changed line*)
  else
    match
    match p.[pp] with
    | '?' ->
        if pp + 1 > String.length p - 1 then None
        else if sp > String.length s - 1 then Some (2, 0)
        else if p.[pp + 1] = s.[sp] then Some (2, 1)
        else Some (2, 0)
    | '*' ->
        if pp + 1 > String.length p - 1 then None
        else Some(2, swalloc_all p.[pp + 1] s sp)
    | '+' ->
        if pp + 1 > String.length p - 1 then None
        else if sp > String.length s - 1 then None
        else if p.[pp + 1] = s.[sp] then
          Some (2, swalloc_all p.[pp + 1] s sp)
        else None
    | c ->
        if sp < String.length s && c = s.[sp] then
          Some (1, 1)
        else
          None
    with
  | None -> (false, sp)                                   (* changed line *)
  | Some (jump_p, jump_s) ->
      at_end p (pp + jump_p) s (sp + jump_s)
```
We can now count the number of disjoint matches as follows:
```ocaml
let match_num_disjoint p s =
  let counter = ref 0 in
  let rec loop n =
    if n > String.length s - 1 then !counter
    else
      let result, until = at_end p 0 s n in
      if result
      then (counter := !counter + 1; loop until)
      else loop (n + 1)
  in
  loop 0
```
