(** First version *)

let rec search_fst p s =
  String.length p <= String.length s
  && (String.sub s 0 (String.length p) = p
     || search_fst p (String.sub s 1 (String.length s - 1)))



(** Second version *)

let rec at p pp s sp l =
  l = 0 || p.[pp] = s.[sp] && at p (pp + 1) s (sp + 1) (l - 1)

let rec search' n p s =
  String.length p <= String.length s - n
  && (at p 0 s n (String.length p) || search' (n + 1) p s)

let search_snd = search' 0



(** The testing *)

let time f =
  let t1 = Sys.time () in
  let _ = f () in
  let t2 = Sys.time () in
  t2 -. t1

let compare pl sl =
  let p = String.make pl 'a' ^ "b" in
  let s = String.make sl 'a' ^ "b" in
  let t_fst = time (fun () -> search_fst p s) in
  let t_snd = time (fun () -> search_snd p s) in
  Printf.printf "fst: %g\nsnd: %g\n" t_fst t_snd
