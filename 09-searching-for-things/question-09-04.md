# Question 9.4

> Add the special symbol `\` to our final search program.
> It indicates that the following symbol is not a special symbol, but a normal character.
> Note that, we must write `'\\'` for that character in OCaml code.

---

We add a match to the function `at`:
```ocaml
let rec at p pp s sp =
  pp > String.length p - 1 ||
  match
    match p.[pp] with
    | '?' ->
        if pp + 1 > String.length p - 1 then None
        else if sp > String.length s - 1 then Some (2, 0)
        else if p.[pp + 1] = s.[sp] then Some (2, 1)
        else Some (2, 0)
    | '*' ->
        if pp + 1 > String.length p - 1 then None
        else Some(2, swalloc_all p.[pp + 1] s sp)
    | '+' ->
        if pp + 1 > String.length p - 1 then None
        else if sp > String.length s - 1 then None
        else if p.[pp + 1] = s.[sp] then
          Some (2, swalloc_all p.[pp + 1] s sp)
        else None
    | '\\' ->                                         (* new match *)
        if pp + 1 > String.length p - 1 then None
        else if sp > String.length s - 1 then None
        else if p.[pp + 1] = s.[sp] then Some (2, 1)
        else None
    | c ->
        if sp < String.length s && c = s.[sp] then
          Some (1, 1)
        else
          None
  with
  | None -> false
  | Some (jump_p, jump_s) ->
      at p (pp + jump_p) s (sp + jump_s)
```

We can test the resulting `search` function as follows:
```ocaml
# search "a?bc" "a?bc";;
- : bool = false

# search "a\\?bc" "a?bc";;
- : bool = true
```
