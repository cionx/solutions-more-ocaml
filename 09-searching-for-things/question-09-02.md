# Question 9.2

> Write functions which return the position and length of the longest prefix of a string in another.
> That is to say, the longest initial part of the pattern which matches anywhere in the string.

---

We modify the function `at` so that instead of returning a boolean, we return how much of the pattern we managed to match:
```ocaml
let rec match_len_at p pp s sp =
  if pp > String.length p - 1 then pp                     (* modified *)
  else
    match
    match p.[pp] with
    | '?' ->
        if pp + 1 > String.length p - 1 then None
        else if sp > String.length s - 1 then Some (2, 0)
        else if p.[pp + 1] = s.[sp] then Some (2, 1)
        else Some (2, 0)
    | '*' ->
        if pp + 1 > String.length p - 1 then None
        else Some(2, swalloc_all p.[pp + 1] s sp)
    | '+' ->
        if pp + 1 > String.length p - 1 then None
        else if sp > String.length s - 1 then None
        else if p.[pp + 1] = s.[sp] then
          Some (2, swalloc_all p.[pp + 1] s sp)
        else None
    | c ->
        if sp < String.length s && c = s.[sp] then
          Some (1, 1)
        else
          None
    with
  | None -> pp                                            (* modified *)
  | Some (jump_p, jump_s) ->
      match_len_at p (pp + jump_p) s (sp + jump_s)
```

We can then implement the described function as follows:
```ocaml
let max_match_len p s =
  let sofar = ref (0, 0) in  (* (index, length) *)
  for n = 0 to String.length s - 1 do
    let len = match_len_at p 0 s n in
    let (_, maxlen) = !sofar in
    if len > maxlen then sofar := (n, len)
  done;
  !sofar
```
