let swalloc_all ch s sp =
  let x = ref sp in
  while !x < String.length s && s.[!x] = ch do x := !x + 1 done;
  !x - sp

let rec at p pp s sp =
  pp > String.length p - 1 ||
  match
    match p.[pp] with
    | '?' ->
        if pp + 1 > String.length p - 1 then None
        else if sp > String.length s - 1 then Some (2, 0)
        else if p.[pp + 1] = s.[sp] then Some (2, 1)
        else Some (2, 0)
    | '*' ->
        if pp + 1 > String.length p - 1 then None
        else Some(2, swalloc_all p.[pp + 1] s sp)
    | '+' ->
        if pp + 1 > String.length p - 1 then None
        else if sp > String.length s - 1 then None
        else if p.[pp + 1] = s.[sp] then
          Some (2, swalloc_all p.[pp + 1] s sp)
        else None
    | c ->
        if sp < String.length s && c = s.[sp] then
          Some (1, 1)
        else
          None
  with
  | None -> false
  | Some (jump_p, jump_s) ->
      at p (pp + jump_p) s (sp + jump_s)

let rec search' n p s =
  (n < String.length s || n = 0 && String.length s = 0)
  && (at p 0 s n || search' (n + 1) p s)

let rec search ?(case_insensitive = false) p s =
  if case_insensitive
  then search (String.lowercase_ascii p) (String.lowercase_ascii s)
  else search' 0 p s
