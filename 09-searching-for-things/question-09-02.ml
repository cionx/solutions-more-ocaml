let swalloc_all ch s sp =
  let x = ref sp in
  while !x < String.length s && s.[!x] = ch do x := !x + 1 done;
  !x - sp

let rec match_len_at p pp s sp =
  if pp > String.length p - 1 then pp                     (* modified *)
  else
    match
    match p.[pp] with
    | '?' ->
        if pp + 1 > String.length p - 1 then None
        else if sp > String.length s - 1 then Some (2, 0)
        else if p.[pp + 1] = s.[sp] then Some (2, 1)
        else Some (2, 0)
    | '*' ->
        if pp + 1 > String.length p - 1 then None
        else Some(2, swalloc_all p.[pp + 1] s sp)
    | '+' ->
        if pp + 1 > String.length p - 1 then None
        else if sp > String.length s - 1 then None
        else if p.[pp + 1] = s.[sp] then
          Some (2, swalloc_all p.[pp + 1] s sp)
        else None
    | c ->
        if sp < String.length s && c = s.[sp] then
          Some (1, 1)
        else
          None
    with
  | None -> pp                                            (* modified *)
  | Some (jump_p, jump_s) ->
      match_len_at p (pp + jump_p) s (sp + jump_s)

let max_match_len p s =
  let sofar = ref (0, 0) in  (* (index, length) *)
  for n = 0 to String.length s - 1 do
    let len = match_len_at p 0 s n in
    let (_, maxlen) = !sofar in
    if len > maxlen then sofar := (n, len)
  done;
  !sofar
