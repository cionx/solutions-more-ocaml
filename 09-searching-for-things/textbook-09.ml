(** No modifiers. *)

let rec at p pp s sp l =
  l = 0 || p.[pp] = s.[sp] && at p (pp + 1) s (sp + 1) (l - 1)

let rec search' n p s =
  String.length p <= String.length s
  && (at p 0 s n (String.length p) || search' (n + 1) p s)

let search = search' 0



(** Modifiers. *)

let swalloc_all ch s sp =
  let x = ref sp in
  while !x < String.length s && s.[!x] = ch do x := !x + 1 done;
  !x - sp

let rec at p pp s sp =
  pp > String.length p - 1 ||   (* whole pattern used - match *)
  match
    match p.[pp] with
    | '?' ->
        if pp + 1 > String.length p - 1 then None         (* end pattern *)
        else if sp > String.length s - 1 then Some (2, 0) (* end string *)
        else if p.[pp + 1] = s.[sp] then Some (2, 1)      (* the character *)
        else Some (2, 0)                                  (* any other character *)
    | '*' ->
        if pp + 1 > String.length p - 1 then None         (* end pattern *)
        else Some(2, swalloc_all p.[pp + 1] s sp)         (* read zero or more *)
    | '+' ->
        if pp + 1 > String.length p - 1 then None         (* end pattern *)
        else if sp > String.length s - 1 then None        (* end string *)
        else if p.[pp + 1] = s.[sp] then
          Some (2, swalloc_all p.[pp + 1] s sp)           (* read one or more *)
        else None                                         (* did not match *)
    | c ->
        if sp < String.length s && c = s.[sp] then
          Some (1, 1)
        else
          None
  with
  | None -> false
  | Some (jump_p, jump_s) ->
      at p (pp + jump_p) s (sp + jump_s)

let rec search' n p s =
  (n < String.length s || n = 0 && String.length s = 0)
  && (at p 0 s n || search' (n + 1) p s)

let search = search' 0
