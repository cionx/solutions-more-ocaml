# Question 9.3

> Compare the speed of the first two versions of `search` given in this chapter.

---

We name the two functions `search_fst` and `search_snd`:
```ocaml
(** First version *)

let rec search_fst p s =
  String.length p <= String.length s
  && (String.sub s 0 (String.length p) = p
     || search_fst p (String.sub s 1 (String.length s - 1)))



(** Second version *)

let rec at p pp s sp l =
  l = 0 || p.[pp] = s.[sp] && at p (pp + 1) s (sp + 1) (l - 1)

let rec search' n p s =
  String.length p <= String.length s - n
  && (at p 0 s n (String.length p) || search' (n + 1) p s)

let search_snd = search' 0
```
We use the following function to time function calls:
```ocaml
let time f =
  let t1 = Sys.time () in
  let _ = f () in
  let t2 = Sys.time () in
  t2 -. t1
```
We use test pattern and test strings of the form `"aaa...aaab"`:
```ocaml
let compare pl sl =
  let p = String.make pl 'a' ^ "b" in
  let s = String.make sl 'a' ^ "b" in
  let t_fst = time (fun () -> search_fst p s) in
  let t_snd = time (fun () -> search_snd p s) in
  Printf.printf "fst: %g\nsnd: %g\n" t_fst t_snd
```

Contrary to expectations, the first function is actually _faster_ than the second one:
```ocaml
# compare 5_000 10_000;;
fst: 0.102158
snd: 2.39367
- : unit = ()
```
We have no idea what’s going on here.
